/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';

import { TestApp } from 'react-native-awesome-module';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

export default function App() {
  return (
    <SafeAreaProvider>
      <SafeAreaView style={{ flex: 1, backgroundColor: 'green' }}>
        <TestApp />
      </SafeAreaView>
    </SafeAreaProvider>
  );
}
