/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const TestApp = () => {
  const { top, bottom } = useSafeAreaInsets();

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Text
        style={{ color: 'red', textAlign: 'center' }}
      >{`React Native\n\ntop = ${top}, bottom = ${bottom}`}</Text>
    </View>
  );
};
